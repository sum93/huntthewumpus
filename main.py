from random import choice, randint, shuffle
from re import match

BAT = "bat"
PIT = "pit"
EMPTY = "empty"
WUMPUS_START = "wumpus start"
PLAYER_START = "player start"
MAX_SHOTS = 5


EXTRA_EDGES = {
    frozenset({1, 5}),
    frozenset({1, 20}),
    frozenset({2, 18}),
    frozenset({3, 16}),
    frozenset({4, 14}),
    frozenset({6, 13}),
    frozenset({7, 20}),
    frozenset({8, 12}),
    frozenset({9, 19}),
    frozenset({10, 17}),
    frozenset({11, 15}),
}

random_id_list = list()
for i in range(1, 21):
    random_id_list.append(i)
shuffle(random_id_list)


def get_next_id():
    global random_id_list
    return random_id_list.pop()


class Room:
    def __init__(self, room_type):
        self.id = get_next_id()
        self.type = room_type


class Map:
    def __init__(self):
        rooms = []

        for _ in range(0, 2):
            rooms.append(Room(BAT))
            rooms.append(Room(PIT))

        for _ in range(0, 14):
            rooms.append(Room(EMPTY))

        rooms.append(Room(WUMPUS_START))
        rooms.append(Room(PLAYER_START))

        self.rooms = rooms

        self.wumpus_location = self.find_room_id_by_type(WUMPUS_START)
        self.player_location = self.find_room_id_by_type(PLAYER_START)

    def get_player_room(self):
        return self.find_room_by_id(self.player_location)

    def find_room_id_by_type(self, room_type):
        for room in self.rooms:
            if room.type == room_type:
                return room.id

    def find_room_by_id(self, room_id):
        for room in self.rooms:
            if room.id == room_id:
                return room

    def random_player_location(self):
        self.player_location = randint(1, 20)

    def wumpus_move(self):
        neighbours = list()
        neighbours.append(self.wumpus_location)
        for room in self.rooms:
            if self.is_neighbour(self.wumpus_location, room.id):
                neighbours.append(room.id)
        self.wumpus_location = choice(neighbours)

    def get_player_neighbours(self):
        neighbours = list()

        for room in self.rooms:
            if self.is_neighbour(self.player_location, room.id):
                neighbours.append(room.id)

        return neighbours

    @staticmethod
    def is_neighbour(room_a_id, room_b_id):
        return abs(room_a_id - room_b_id) == 1 or frozenset({room_a_id, room_b_id}) in EXTRA_EDGES


class Game:
    def __init__(self):
        self.map = Map()
        self.remaining_shot = MAX_SHOTS

    def next_turn(self):
        print("You are in room " + str(self.map.player_location))
        player_neighbours = self.map.get_player_neighbours()
        self.describe_surroundings_randomly(player_neighbours)
        print("You can move to the following rooms: " + str(player_neighbours))

        command = self.get_input()
        command_type, destination = command.split()
        destination = int(destination)

        if command_type == 'M':
            if destination not in player_neighbours:
                print("You can't go there!")
            else:
                self.map.player_location = destination
                self.on_room_arrival()

        if command_type == 'S':
            if destination not in player_neighbours:
                print("You can't shoot there!")
            else:
                self.on_shoot(destination)

        print("\n")

    def describe_surroundings_randomly(self, neighbours):
        descriptions = list()

        for room_id in neighbours:
            room = self.map.find_room_by_id(room_id)
            if room.type == BAT:
                descriptions.append("You hear some bats.")
            if room.id == self.map.wumpus_location:
                descriptions.append("You hear the Wumpus.")
            if room.type == PIT:
                descriptions.append("You hear wind howling.")

        shuffle(descriptions)

        for description in descriptions:
            print(description)

    def on_room_arrival(self):
        room = self.map.get_player_room()

        if room.id == self.map.wumpus_location:
            print("The Wumpus have brutally eaten you Q u Q")
            self.game_over()
        elif room.type == PIT:
            print("You have fallen into a pit O o O")
            self.game_over()
        elif room.type == BAT:
            print("A group of bats have moved you O ,, O\n")
            self.map.random_player_location()
            self.on_room_arrival()

    def on_shoot(self, target_room):
        room = self.map.find_room_by_id(target_room)
        self.remaining_shot -= 1

        if room.id == self.map.wumpus_location:
            print("You killed the Wumpus ^ u ^")
            self.game_over()
        elif self.remaining_shot == 0:
            print("You ran out of arrows. The Wumpus gonna get you X _ X")
            self.game_over()
        else:
            self.map.wumpus_move()
            print("The Wumpus have moved.")
            self.on_room_arrival()

    def get_input(self):
        command = input("Are you gonna SHOOT (S x) or MOVE (M x)?\n").upper()
        if self.verify_input(command) is None:
            print("You gave an invalid action!")
            return self.get_input()
        return command

    @staticmethod
    def verify_input(command):
        pattern = "^[SM] [0-9]{1,2}$"
        return match(pattern, command)

    @staticmethod
    def game_over():
        exit()


game = Game()

while True:
    game.next_turn()
